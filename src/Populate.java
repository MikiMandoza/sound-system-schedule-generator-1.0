import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;

class Populate {
    private final Random random = new Random();
    private int availableMembers;
    private int[][] scheduleGrid;
    private final int STAGE       = 0;
    private final int ROUND1_ROW1 = 1;
    private final int ROUND1_ROW2 = 2;
    private final int ROUND2_ROW1 = 3;
    private final int ROUND2_ROW2 = 4;
    private final int HALL2       = 5;
    private ArrayList<Member> members = new ArrayList<>();

    Populate(int terms) {
        try {
            availableMembers = Member.getDao().queryForAll().size();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        // 'term' decides the minimum number of times a member plays a role
        scheduleGrid = new int[availableMembers * terms][6];
        initializeGrid();
        fillMembersArray();
    }

    // -------------- COLUMN POPULATING METHODS --------------
    private void stage() {
        int SAMembersCount = getMembersCount(Role.STAGE); // SA = Stage Available
        int selectedMember;

        for (int day = 0; day < scheduleGrid.length; day++) {
            do {
                selectedMember = getMember(Role.STAGE);
                scheduleGrid[day][STAGE] = selectedMember;
            }
            while (repeatsBeforeNextTerm(day, STAGE, SAMembersCount, 1));

            if (day % 2 != 0) // odd index = Sunday meeting
                if (members.get(selectedMember).hasSundayException())
                    swapMembers(day, STAGE);
        }
    }

    private void firstRoundRow(Role role) {
        int FRMemberCount = getMembersCount(Role.MICROPHONE);
        int selectedMember;

        switch (role) {
            case FIRST_ROUND_1ST_ROW:
                for (int day = 0; day < scheduleGrid.length; day++)
                    do {
                        selectedMember = getMember(Role.MICROPHONE);
                        scheduleGrid[day][ROUND1_ROW1] = selectedMember;
                        if (allOccupied(day, ROUND1_ROW1, FRMemberCount, 1)) {
                            repeatAMember(day, ROUND1_ROW1, Role.MICROPHONE);
                            break;
                        }
                    }
                    while (
                            isAssignedTo(selectedMember, Role.STAGE, day) ||
                            repeatsBeforeNextTerm(day, ROUND1_ROW1, FRMemberCount, 1)
                          );
                break;
            case FIRST_ROUND_2ND_ROW:
                for (int day = 0; day < scheduleGrid.length; day++)
                    do {
                        selectedMember = getMember(Role.MICROPHONE);
                        scheduleGrid[day][ROUND1_ROW2] = selectedMember;
                        if (allOccupied(day, ROUND1_ROW2, FRMemberCount, 1)) {
                            repeatAMember(day, ROUND1_ROW2, Role.MICROPHONE);
                            break;
                        }
                    }
                    while (
                            isAssignedTo(selectedMember, Role.STAGE, day)               ||
                            isAssignedTo(selectedMember, Role.FIRST_ROUND_1ST_ROW, day) ||
                            repeatsBeforeNextTerm(day, ROUND1_ROW2, FRMemberCount, 1)
                          );
                break;
        }
    }

    private void secondRoundRow(Role role) {
        int FRMemberCount = getMembersCount(Role.MICROPHONE);
        int selectedMember;

        switch (role) {
            case SECOND_ROUND_1ST_ROW:
                for (int day = 0; day < scheduleGrid.length; day++)
                    do {
                        selectedMember = getMember(Role.MICROPHONE);
                        scheduleGrid[day][ROUND2_ROW1] = selectedMember;
                        if (allOccupied(day, ROUND2_ROW1, FRMemberCount, 1)) {
                            repeatAMember(day, ROUND2_ROW1, Role.MICROPHONE);
                            break;
                        }
                    }
                    while (
                            isAssignedTo(selectedMember, Role.STAGE, day)               ||
                            isAssignedTo(selectedMember, Role.FIRST_ROUND_1ST_ROW, day) ||
                            isAssignedTo(selectedMember, Role.FIRST_ROUND_2ND_ROW, day) ||
                            repeatsBeforeNextTerm(day, ROUND2_ROW1, FRMemberCount, 1)
                          );
                break;
            case SECOND_ROUND_2ND_ROW:
                for (int day = 0; day < scheduleGrid.length; day++)
                    do {
                        selectedMember = getMember(Role.MICROPHONE);
                        scheduleGrid[day][ROUND2_ROW2] = selectedMember;
                        if (allOccupied(day, ROUND2_ROW2, FRMemberCount, 1)) {
                            repeatAMember(day, ROUND2_ROW2, Role.MICROPHONE);
                            break;
                        }
                    }
                    while (
                            isAssignedTo(selectedMember, Role.STAGE, day)                   ||
                            isAssignedTo(selectedMember, Role.FIRST_ROUND_1ST_ROW, day)     ||
                            isAssignedTo(selectedMember, Role.FIRST_ROUND_2ND_ROW, day)     ||
                            isAssignedTo(selectedMember, Role.SECOND_ROUND_1ST_ROW, day)    ||
                            repeatsBeforeNextTerm(day, ROUND2_ROW2, FRMemberCount, 1)
                          );
                break;
        }
    }

    private void secondHall() {
        int SHAMemberCount = getMembersCount(Role.SECOND_HALL);
        int selectedMember;

        for (int day = 0; day < scheduleGrid.length; day += 2)
            do {
                selectedMember = getMember(Role.SECOND_HALL);
                scheduleGrid[day][HALL2] = selectedMember;
                if (allOccupied(day, HALL2, SHAMemberCount, 2)) {
                    repeatAMember(day, HALL2, Role.SECOND_HALL);
                    break;
                }
            }
            while (
                    isAssignedTo(selectedMember, Role.STAGE, day)               ||
                    isAssignedTo(selectedMember, Role.FIRST_ROUND_1ST_ROW, day) ||
                    isAssignedTo(selectedMember, Role.FIRST_ROUND_2ND_ROW, day) ||
                    repeatsBeforeNextTerm(day, HALL2, SHAMemberCount, 2)
                  );
    }
// -------------- ASSERTION METHODS --------------
    private boolean repeatsBeforeNextTerm(int day, int role, int dayOrMemCount, int skip) {
        int appearCount = 1;
        int stopDay = day - skip * (dayOrMemCount - 1);

        for (int i = day - skip; i >= stopDay; i -= skip) {
            if (i < 0) break;
            if (scheduleGrid[i][role] == scheduleGrid[day][role])
                ++appearCount;
        }
        return appearCount > 1;
    }

    private boolean isAssignedTo(int selectedMember, Role role, int day) {
        boolean check = false;

        switch (role) {
            case STAGE:
                check = (selectedMember == scheduleGrid[day][STAGE]);
                break;
            case FIRST_ROUND_1ST_ROW:
                check = (selectedMember == scheduleGrid[day][ROUND1_ROW1]);
                break;
            case FIRST_ROUND_2ND_ROW:
                check = (selectedMember == scheduleGrid[day][ROUND1_ROW2]);
                break;
            case SECOND_ROUND_1ST_ROW:
                check = (selectedMember == scheduleGrid[day][ROUND2_ROW1]);
                break;
            case SECOND_ROUND_2ND_ROW:
                check = (selectedMember == scheduleGrid[day][ROUND2_ROW2]);
                break;
            case SECOND_HALL:
                check = (selectedMember == scheduleGrid[day][HALL2]);
                break;
        }
        return check;
    }

    private boolean allOccupied(int day, int role, int dayOrMemCount, int skip) {
        boolean areOccupied = true;
        int stopDay = day - skip * (dayOrMemCount - 1);
        if (stopDay < 0) stopDay = 0;
        int[] allIndex = new int[role + dayOrMemCount];
        int iterator = 0;
        // add same day members
        for (; iterator < role; iterator++)
            allIndex[iterator] = scheduleGrid[day][iterator];
        // add same term (role) members
        for (int i = day; i >= stopDay; i -= skip) {
            allIndex[iterator] = scheduleGrid[i][role];
            ++iterator;
        }
        // check if all members are in allIndex
        for (int i = 0; i < members.size(); i++) {
            int finalI = i;
            areOccupied = areOccupied && IntStream.of(allIndex).anyMatch(x -> x == finalI);
        }
        return areOccupied;
    }

    private void repeatAMember(int day, int roleColumn, Role role) {
        int replacingMember = getMember(role);
        while (
                isAssignedTo(replacingMember, Role.STAGE, day)                ||
                isAssignedTo(replacingMember, Role.FIRST_ROUND_1ST_ROW, day)  ||
                isAssignedTo(replacingMember, Role.FIRST_ROUND_2ND_ROW, day)  ||
                isAssignedTo(replacingMember, Role.SECOND_ROUND_1ST_ROW, day) ||
                isAssignedTo(replacingMember, Role.SECOND_ROUND_2ND_ROW, day) ||
                isAssignedTo(replacingMember, Role.SECOND_HALL, day)
              )
            replacingMember = getMember(role);
        scheduleGrid[day][roleColumn] = replacingMember;
    }

    // --------------   OTHER METHODS   --------------
    private void fillMembersArray() {
        for (Member member : Member.getDao())
            members.add(member);
    }

    private int getMember(Role role) {
        int selectedMember = -1;

        switch (role) {
            case STAGE:
                while (true) {
                    selectedMember = random.nextInt(availableMembers);
                    if (members.get(selectedMember).canBeStage())
                        break;
                }
                break;
            case MICROPHONE:
                while (true) {
                    selectedMember = random.nextInt(availableMembers);
                    if (members.get(selectedMember).canRotateMic())
                        break;
                }
                break;
            case SECOND_HALL:
                while (true) {
                    selectedMember = random.nextInt(availableMembers);
                    if (members.get(selectedMember).canBeSecondHall())
                        break;
                }
        }
        return selectedMember;
    }

    private int getMembersCount(Role role) {
        int membersCount = 0;
        switch (role) {
            case STAGE:
                for (Member member : members)
                    if (member.canBeStage())
                        membersCount += 1;
                break;
            case MICROPHONE:
                for (Member member: members)
                    if (member.canRotateMic())
                        membersCount += 1;
                break;
            case SECOND_HALL:
                for (Member member: members)
                    if (member.canBeSecondHall())
                        membersCount += 1;
                break;
            default:
                membersCount = -1;
        }
        return membersCount;
    }

    private void swapMembers(int day, int role) {
        int temp = scheduleGrid[day - 1][role];
        scheduleGrid[day - 1][role] = scheduleGrid[day][role];
        scheduleGrid[day][role] = temp;
    }

    private void initializeGrid() {
        for (int day = 0; day < scheduleGrid.length; day++)
            for (int role = 0; role < 6; role++)
                scheduleGrid[day][role] = -1;
    }

    // the class that creates the excel file needs this array
    String[][] getScheduleGrid() {
        String[][] names = new String[scheduleGrid.length][6];
        stage();
        System.out.println("stage populated...");
        firstRoundRow(Role.FIRST_ROUND_1ST_ROW);
        System.out.println("round1 row1 populated...");
        firstRoundRow(Role.FIRST_ROUND_2ND_ROW);
        System.out.println("round1 row2 populated...");
        secondRoundRow(Role.SECOND_ROUND_1ST_ROW);
        System.out.println("round2 row1 populated...");
        secondRoundRow(Role.SECOND_ROUND_2ND_ROW);
        System.out.println("round2 row2 populated...");
        secondHall();
        System.out.println("secondHall populated...");

        for (int i = 0; i < names.length; i++)
            for (int j = 0; j < 6; j++) {
                if (scheduleGrid[i][j] == -1) continue;
                names[i][j] = members.get(scheduleGrid[i][j]).getFirstName();
            }
        return names;
    }

/* this is only a check-up method. Delete it after the program has been fully built
    private void showSchedule(Object[][] twoDArray) {
        for (Object[] aScheduleGrid : twoDArray) {
            for (int column = 0; column < 6; column++)
                System.out.print(aScheduleGrid[column] + " ");
            System.out.println();
        }
    }*/
}
