import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

@DatabaseTable
class Member {
    private static Dao<Member, Integer> memberDao;

    static {
        try {
            memberDao = DaoManager.createDao(DBConnection.getConnectionSource(), Member.class);
            TableUtils.createTableIfNotExists(DBConnection.getConnectionSource(), Member.class);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @DatabaseField
    private String firstName;
    @DatabaseField
    private String lastName;
    @DatabaseField(columnName = "id", id = true)
    private int memberID;
    @DatabaseField
    private boolean stage;
    @DatabaseField
    private boolean rotateMic;
    @DatabaseField
    private boolean secondHall;
    @DatabaseField
    private boolean hasSundayException;

    // a no-arg constructor (required by ORMLITE - I don't know the reason)
    Member() {
    }

    static Dao<Member, Integer> getDao() {
        return memberDao;
    }

    String getFirstName() {
        return firstName;
    }

    void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    String getLastName() {
        return lastName;
    }

    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    int getMemberID() {
        return memberID;
    }

    void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    boolean canBeStage() {
        return stage;
    }

    void setStage(boolean stage) {
        this.stage = stage;
    }

    boolean canRotateMic() {
        return rotateMic;
    }

    void setRotateMic(boolean rotateMic) {
        this.rotateMic = rotateMic;
    }

    boolean canBeSecondHall() {
        return secondHall;
    }

    void setSecondHall(boolean secondHall) {
        this.secondHall = secondHall;
    }

    boolean hasSundayException() {
        return hasSundayException;
    }

    void setHasSundayException(boolean hasSundayException) {
        this.hasSundayException = hasSundayException;
    }

    void save() {
        try {
            memberDao.create(this);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
