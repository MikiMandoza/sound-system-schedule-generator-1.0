import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import java.time.LocalDateTime;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
class WordFileGenerator {

    private WordFileGenerator() {
        Populate populate = new Populate(4);
        String[][] names  = populate.getScheduleGrid();
        // month to name map to translate a month's value (int) into an Amharic String
        Map<Integer, String> AMMonths = new HashMap<>(12);
        AMMonths.put(1, "ጥር");
        AMMonths.put(2, "የካቲት");
        AMMonths.put(3, "መጋቢት");
        AMMonths.put(4, "ሚያዝያ");
        AMMonths.put(5, "ግንቦት");
        AMMonths.put(6, "ሰኔ");
        AMMonths.put(7, "ሐምሌ");
        AMMonths.put(8, "ነሐሴ");
        AMMonths.put(9, "መስከረም");
        AMMonths.put(10, "ጥቅምት");
        AMMonths.put(11, "ህዳር");
        AMMonths.put(12, "ታህሳሥ");

        // creating the Word document file
        XWPFDocument doc = new XWPFDocument();

        // creating the schedule table
        XWPFTable table  = doc.createTable();

        // writing the column titles
        XWPFTableRow headerRow = table.createRow();
        headerRow.addNewTableCell().setText("ሳምንት");
        headerRow.addNewTableCell().setText("ቀን");
        headerRow.addNewTableCell().setText("መድረክ");
        // merge 1st round cells
        headerRow.addNewTableCell();
        headerRow.addNewTableCell().setText("በመጀመሪያው ዙር");
        // merge 2nd round cells
        headerRow.addNewTableCell();
        headerRow.addNewTableCell().setText("በለተኛው ዙር");
        headerRow.addNewTableCell().setText("በሁለተኛው አዳራሽ");

        LocalDateTime midWeek = LocalDateTime.of(2018, 8, 10, 0, 0);
        String weekMonth, monthOnSunday;

        // populating the table with names and date information begins here
        for (String[] name : names) {
            // each day is a row, hence the XWPFTableRow
            XWPFTableRow dayRow = table.createRow();
            // the beginning and end days of the week may reside in different months.
            // To include the name of the next month in the cell, the month after
            // 7 days must be calculated and compared with the month on Monday
            weekMonth     = AMMonths.get(midWeek.getMonthValue());
            monthOnSunday = AMMonths.get(midWeek.plusDays(6).getMonthValue());
            // here, the months are compared and the appropriate date format is put
            if (weekMonth.equals(monthOnSunday))
                dayRow.addNewTableCell().setText(weekMonth + " " + midWeek.getDayOfMonth() + " - " + midWeek.plusDays(6).getDayOfMonth());
            else
                dayRow.addNewTableCell().setText(weekMonth + " " + midWeek.getDayOfMonth() + " - " + monthOnSunday + " " + midWeek.plusDays(6).getDayOfMonth());
            // In the next cell the name of the day will be placed
            dayRow.addNewTableCell(); // add the name of the day in this cell
            // The following for loop will create new cells and populate them with names
            for (int j = 0; j < 6; j++)
                dayRow.addNewTableCell().setText(name[j]);
            // the date object must be increased by 7 days on every iteration to get next Monday's date
            midWeek = midWeek.plusDays(7);
        }

        try {
            FileOutputStream out = new FileOutputStream(new File("soundSchedule.docx"));
            doc.write(out);
            out.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
